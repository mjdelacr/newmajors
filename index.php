<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>New Majors | Loyola University New Orleans</title>
    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/da5e7302-b0c5-44a6-a7cb-b1e4e78ad09d.css" />
    <!-- Theme CSS -->
    <link href="css/agency.min.css" rel="stylesheet">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="./apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="./android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">
    <link rel="manifest" href="./manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
    <![endif]-->
</head>

<body id="page-top" class="index">
    <!-- Header -->
    <header>
        <div class="container">
            <a class="logo" href="http://www.loyno.edu"><img src="img/Loyola_Wordmark_Horizontal.png" alt="Loyola University New Orleans" class="img-responsive" /></a>
            <div class="hero-image col-lg-12 text-center"><img src="img/NewMajors_Hero_w.png" alt="Loyola University New Orleans Summer Camps" class="img-responsive"></div>
        </div>
    </header>
    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="bg-yellow">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">New Majors at Loyola</h2>
                    <h3 class="section-heading">Explore Our Programs!</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="http://cas.loyno.edu/mathematics/bachelor-science-computer-science">
                        <img src="img/ComputerScience.jpg" class="img-responsive" alt="Computer Science">
                    </a>
                    <div class="portfolio-caption">
                        <h4>
                            <a href="http://cas.loyno.edu/mathematics/bachelor-science-computer-science">Computer Science</a>
                        </h4>

                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="http://cas.loyno.edu/mathematics/bachelor-science-computer-science-game-programming">
                        <img src="img/ComputerScience_GameProgramming.jpg" class="img-responsive" alt="Computer Science with Game Programming">
                    </a>
                    <div class="portfolio-caption">
                        <h4>
                            <a href="http://cas.loyno.edu/mathematics/bachelor-science-computer-science-game-programming">Computer Science with Game Programming</a>
                        </h4>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="http://cas.loyno.edu/food-studies">
                        <img src="img/FoodStudies.jpg" class="img-responsive" alt="Kids Art Summer Camp">
                    </a>
                    <div class="portfolio-caption">
                        <h4>
                            <a href="http://cas.loyno.edu/food-studies">Food Studies</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#">
                        <img src="img/ComputerScience.jpg" class="img-responsive" alt="Interactive Design">
                    </a>
                    <div class="portfolio-caption">
                        <h4>
                            <a href="#">Interactive Design</a>
                        </h4>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="http://cas.loyno.edu/part-time/certificate-translation-and-interpreting">
                        <img src="img/TranslationInterpreting.jpg" class="img-responsive" alt="Translation &amp; Intepreting">
                    </a>
                    <div class="portfolio-caption">
                        <h4>
                            <a href="http://cas.loyno.edu/part-time/certificate-translation-and-interpreting">Translation &amp; Interpreting</a>
                        </h4>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="http://cas.loyno.edu/masscomm/visual-communication">
                        <img src="img/VisualCommunication.jpg" class="img-responsive" alt="Visual Communication">
                    </a>
                    <div class="portfolio-caption">
                        <h4>
                            <a href="http://cas.loyno.edu/masscomm/visual-communication ">Visual Communication</a>
                        </h4>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <footer class="bg-maroon">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="img/stacked-logo.png" alt="Loyola University New Orleans" class="img-responsive" />
                </div>
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; 1996-<?php echo date('Y');?></span>
                    <p>Loyola University New Orleans
                        <br /> 6363 St. Charles Avenue | New Orleans, LA 70118</p>
                </div>
                <div class="col-md-4 text-center">
                    <ul class="list-inline social-buttons">
                        <li><a href="http://www.facebook.com/loyno"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="http://www.flickr.com/photos/loyolanola/"><i class="fa fa-flickr"></i></a></li>
                        <li><a href="http://twitter.com/loyola_NOLA"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="http://instagram.com/loyola_nola/"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js "></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js "></script>
    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js " integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb " crossorigin="anonymous "></script>
    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js "></script>
    <script src="js/contact_me.js "></script>
    <!-- Theme JavaScript -->
    <script src="js/agency.min.js "></script>
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-2131369-16', 'auto');
    ga('send', 'pageview');
    </script>
</body>

</html>
